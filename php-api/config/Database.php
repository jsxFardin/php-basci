<?php
/**
 * Created by PhpStorm.
 * User: fssha
 * Date: 2/25/2019
 * Time: 12:53 PM
 */

use PDO, PDOException;

class Database
{
    private $host = 'localhost';
    private $db = 'lumen-store';
    private $userName = 'root';
    private $pass = '';
    public $conn;

    public function  __construct()
    {

    }

    public function connection()
    {
        try{
            // $this->conn = new PDO("mysql:host=$this->host;dbname=$this->db",$this->username,$this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (PDOException $e){
            echo "Database connection Failed".$e->getMessage();
        }

        return $this->conn;
    }

}