@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-stats card-raised">
                <div class="card-body text-center">
                    <span class="page-header-text">VR Cash</span>
                </div>
            </div>
        </div>
    </div>

    {{--edit--}}
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-4 col-lg-4">
                    <span class="page-title-text">List</span>
                </div>
                <div class="col-md-8 col-lg-8">
                    <button id="modal" class="btn btn-round btn-info pull-right"
                            title="Add New"
                            data-title="Add New VR Cash"
                            data-toggle="modal" data-target="#modalForm">
                        <i class="fa fa-plus"></i>&nbsp;Add New
                    </button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-center">
                <ul class="bg-warning text-white"></ul>
                <table class="table table-bordered table-striped table-center" cellspacing="0">
                    <thead class="bg-info">
                    <tr class="bg-info text-white">
                        <th width="100">Ser No.</th>
                        <th width="500">VR Cash</th>
                        <th width="800">Description</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody id="listItems"></tbody>
                </table>

                @include('layouts.partials.pagination')

                <div align="center" class="form-row d-flex justify-content-center">
                    <div class="form-group col-md-4 mt-2">
                        <button type="button" class="btn btn-info btn-md btn-block">Preview</button>
                    </div>
                    <div class="form-group col-md-4 mt-3">
                        <select id="inputState" class="form-control">
                            <option selected>PDF</option>
                            <option>PDF</option>
                            <option>PDF</option>
                        </select>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="modalFormTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add New VR Cash</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div id="messages">
                        <ul></ul>
                    </div>

                    <input type="hidden" name="vrnumber" id="vrNumber">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">VR No Cash</label>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Description</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <input type="text" name="vrnocash" id="vrNoCash" class="form-control" placeholder="VR Cash"
                                   required>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="description" id="description" class="form-control"
                                   placeholder="Description" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="close" class="btn btn-round btn-danger font-12" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" id="create" class="btn btn-round btn-info font-12">Create</button>
                    <button type="button" id="update" class="btn btn-round btn-info font-12 d-none">Update</button>
                </div>
            </div>
        </div>
    </div>

    {{--single view modal--}}
    <div class="modal fade" id="showItem" tabindex="-1" role="dialog" aria-labelledby="modalFormTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    {{--<div id="messages">--}}
                        {{--<ul></ul>--}}
                    {{--</div>--}}

                    <div class="row">
                        <div class="col-md-12 details">

                            {{--<div class=" vrnocash"></div>--}}
                            {{--<div class=" description"></div>--}}

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="close" class="btn btn-round btn-danger font-12" data-dismiss="modal">
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('/js/pagination.js') }}"></script>
<script>
    $(document).ready(function () {
        index()
    })

    $('#modal').on('click', function () {
        $('.modal-body').find("input[type=text]").val("")
    })

    $(document).on('click', '#create', function () {
        create();
    })

    $(document).on('click', '.editItem', function () {
        edit($(this).attr("data-id"))
    })

    $(document).on('click', '.showItem', function () {
        $('#showItem').modal();
        show($(this).attr("data-id"))
    })

    $(document).on('click', '#update', function () {
        update();
    })

    $(document).on('click', '.deleteItem', function () {
        deleteItem($(this).attr("data-id"))
    })

    function index(page_url) {
        page_url = page_url || 'api/vr-cash'
        $("#save").attr("id", "create");
        $('#listItems').html('');
        axios.get(page_url)
            .then(function (response, data) {
                console.log(response.data);
                //  handle success
                let i = 0;
                $.each(response.data.data, function (index, value) {
                    $('#listItems').append(
                        '<tr>' +
                        '<td>' + ++i + '</td>' +
                        '<td>' + value.vrnocash + '</td>' +
                        '<td>' + value.description + '</td>' +
                        '<td class="text-right" width="150">' +
                        '<button class="btn btn-round btn-sm btn-info btn-icon like showItem"  data-id="' + value.vrnocash + '" data-toggle="tooltip" data-placement="top" title="Show Details" ><i class="fa fa-eye"></i></button>' +
                        '<button class="btn btn-round btn-sm btn-warning btn-icon editItem"  data-id="' + value.vrnocash + '" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-pencil"></i></button>' +
                        '<button class="btn btn-round btn-sm btn-danger btn-icon deleteItem" data-id="' + value.vrnocash + '" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-trash"></i></button>' +
                        '</td>' +
                        '</tr>'
                    )
                })
                // $("#create").attr("id", "save");
                makePagination('api/vr-cash', response.data.current_page, response.data.last_page,)
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
    }



    function create() {
        var data = {
            vrnocash: $('#vrNoCash').val(),
            description: $('#description').val(),
        }
        axios.post('/api/vr-cash/create', data)
            .then(response => {
                console.log(response)
                index()
                $('#close').trigger('click');
                swal({
                    title: "Good job!",
                    text: "Successfully Created!",
                    icon: "success",
                    button: "Close",
                });

            }).catch(function (error) {
            $('#messages ul').html('')
            // handle error
            console.log(error.response.data.invalid_fields);
            $.each(error.response.data.invalid_fields, function (index, value) {
                $('#messages ul').append('<li>' + value + '</li>')
                console.log(value)
            })
        });
    }

    function edit(id) {
        axios.get('/api/vr-cash/' + id)
            .then(function (response, data) {
                $("#create").addClass("d-none");
                $("#update").removeClass("d-none");
                // $("#create").attr("id", "save");
                $('#modal').trigger('click');
                $('#vrNumber').val(response.data.data.vrnocash)
                $('#vrNoCash').val(response.data.data.vrnocash)
                $('#description').val(response.data.data.description)
                console.log(response);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
    }

    function show(id) {

        axios.get('/api/vr-cash/' + id)
            .then(function (response, data) {
                var details = '';
                details += `VR NO : ${response.data.data.vrnocash}`;
                details += `<br>Description : ${response.data.data.description}`;
                $('.details').html(details)
                console.log(response);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
    }

    function update() {
        var id = $('#vrNumber').val();
        var data = {
            vrnocash: id,
            vrnocash: $('#vrNoCash').val(),
            description: $('#description').val(),
        }
        axios.put('/api/vr-cash/' + id, data)
            .then(response => {
                $("#create").removeClass("d-none");
                $("#update").addClass("d-none");
                index()
                $('#close').trigger('click');
                swal({
                    title: "Good job!",
                    text: "Successfully Updated!",
                    icon: "success",
                    button: "Close",
                });
            })
    }

    function destroy(id) {
        axios.delete('/api/vr-cash/' + id)
            .then(response => {
                console.log(response)
                $('#close').trigger('click');
                swal({
                    title: "Good job!",
                    text: "Successfully Deleted!",
                    icon: "success",
                    button: "Close",
                });
                index()
            })
    }

    function deleteItem(id) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                destroy(id)
            } else {
                swal("Your imaginary file is safe!");
            }
        });
    }
</script>
@endpush
