<?php


$array = array('lastname', 'email', 'phone');
$comma_separated = implode(",", $array);

// var_dump($array);

// echo $comma_separated; // lastname,email,phone

// Empty string when using an empty array:
// var_dump(implode('hello', array())); // string(0) ""

$pizza  = "piece1,piece2,piece3,piece4,piece5,piece6";
$pieces = explode(",", $pizza);
// echo $pieces[0]; // piece1
// echo $pieces[1]; // piece2

var_dump($pieces);
